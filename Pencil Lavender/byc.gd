extends Area2D

var speed = 200;
var velocity = Vector2()
var screensize = Vector2(480, 720)

# Called when the node enters the scene tree for the first time.
func _ready():
	velocity.x = rand_range(-1,1)
	velocity.y = rand_range(-1,1)
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#if (position.x <= 0) or (position.x >= screensize.x):
	#	velocity.x = -velocity.x
	#if (position.y <= 0) or (position.y >= screensize.y):
	#	velocity.y = -velocity.y
	if position.x <= 0:
		position.x += screensize.x
	if position.x >= screensize.x:
		position.x -= screensize.x
	if position.y <= 0:
		position.y += screensize.y
	if position.y >= screensize.y:
		position.y -= screensize.y
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
		$AnimatedSprite.flip_h = velocity.x > 0
	position += velocity * delta


func _on_byc_area_entered(area):
	#queue_free()
	if (area.get_name() == 'Player'):
		get_parent().queue_free()
