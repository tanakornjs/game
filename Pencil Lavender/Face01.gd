extends Area2D

var speed = 50;
var velocity = Vector2()
var screensize = Vector2(480, 720)
var face_type

func _ready():
	velocity.x = rand_range(-1,1)
	velocity.y = rand_range(-1,1)
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
	face_type = randi() % 3
	match face_type:
		0:
			$AnimatedSprite.animation = "smiley1"
		1:
			$AnimatedSprite.animation = "smiley2"
		2:
			$AnimatedSprite.animation = "Le_lenny"

func _process(delta):
	rotation_degrees += randi() % 3 + 1
	if (position.x <= 0) or (position.x >= screensize.x):
		velocity.x = -velocity.x
	if (position.y <= 0) or (position.y >= screensize.y):
		velocity.y = -velocity.y
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
	position += velocity * delta
	#position.x = clamp(position.x, 0, screensize.x)
	#position.y = clamp(position.y, 0, screensize.y)
	
func _on_Face01_area_entered(area):
	#queue_free()
	if (area.get_name() == 'Player'):
		get_parent().queue_free()

