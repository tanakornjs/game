extends Node2D

const flower = preload("res://flower.tscn")
const face01 = preload("res://face01.tscn")
const byc = preload("res://byc.tscn")
const node2d = preload("res://Node2D.tscn")
var screensize = Vector2(480, 720)

var score = 0
var hi_score = 0
var time = 30

var scene = "Title"
var move_player = false
var cen_x = screensize.x / 2
var cen_y = screensize.y / 2

# Result
var score_run = 0
var new_hi_score = false
var count_pencil = [0,0,0]
var count_pencil_run = [0,0,0]
var result_complete = false

# Called when the node enters the scene tree for the first time.
func _ready():
	screensize = get_viewport().get_visible_rect().size
	$bgm1.play()
	
func spawn_flower():
	#print_debug($FlowerContainer.get_children().size())
	if $FlowerContainer.get_child_count() < 5:
		var c = flower.instance()
		$FlowerContainer.add_child(c)
		c.position = Vector2(rand_range(0, screensize.x), rand_range(0, screensize.y))

func spawn_face():
	if $FaceContainer.get_child_count() < 3:
		var d = node2d.instance()
		var c = face01.instance()
		d.add_child(c)
		$FaceContainer.add_child(d)
		c.position = Vector2(rand_range(0, screensize.x), rand_range(0, screensize.y))

func spawn_byc():
	if $BycContainer.get_child_count() < 2:
		var d = node2d.instance()
		var c = byc.instance()
		d.add_child(c)
		$BycContainer.add_child(d)
		c.position = Vector2(rand_range(0, screensize.x), rand_range(0, screensize.y))
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if scene == "Title":
		$Text/LabelScore.text = "Hi-Score:"
		$Text/LabelScore_num.text = str(int(hi_score))
	if move_player:
		_movePlayer()
		return
	if scene == "Play":
		$Text/LabelTime_num.text = str(int(time))
		$Text/LabelScore.text = "Score:"
		$Text/LabelScore_num.text = str(int(score))
		time -= delta
		if time <= 0:
			scene = "End"
			score_run = 0
			count_pencil_run = [0,0,0]
			$EndGame/Plane.visible = true
			$seResult.play()
	if scene == "End":
		update_result()
	spawn_flower()
	spawn_face()
	spawn_byc()

func _on_btnNewGame_pressed():
	#scene = "Play"
	score = 0
	time = 30
	$Text/TitleName.hide()
	$Text/btnNewGame.hide()
	$Text/LabelName.hide()
	$Text/Help.hide()
	move_player = true
	$Player.z_index = 0
	$Text/LabelScore.text = "Score:"
	$Text/LabelScore_num.text = str(int(score))
	count_pencil = [0,0,0]
	$seNewgame.play()
	
	
func _movePlayer():
	#print_debug($Player.position.y - cen_y)
	if $Player.position.x < cen_x:
		$Player.position.x += 4
	if $Player.position.x > cen_x:
		$Player.position.x -= 4
	if $Player.position.y < cen_y:
		$Player.position.y += 4
	if $Player.position.y > cen_y:
		$Player.position.y -= 4
	if ($Player.position.x - cen_x) < 4.0:
		$Player.position.x = cen_x
	if ($Player.position.y - cen_y) < 4.0:
		$Player.position.y = cen_y
	if $Player.position.x == cen_x && $Player.position.y == cen_y:
		move_player = false
		scene = "Play"

func _on_Player_area_entered(area):
	if scene == "Play":
		if (area.get_name() == 'flower'):
			score += 100
			count_pencil[0] += 1
			$hitflower.play()
			#print_debug("Flower "+str(score))
		if (area.get_name() == 'Face01'):
			score += 200
			count_pencil[1] += 1
			$hitface.play()
			#print_debug("Face "+str(score))
		if (area.get_name() == 'byc'):
			score += 400
			count_pencil[2] += 1
			$hitbyc.play()
			#print_debug("Byc "+str(score))
		

func update_result():
	$EndGame/Plane/LabelScore_num2.text = str(score_run)
	$EndGame/Plane/LabelCount1.text = str(count_pencil_run[0])
	$EndGame/Plane/LabelCount2.text = str(count_pencil_run[1])
	$EndGame/Plane/LabelCount3.text = str(count_pencil_run[2])
	if score_run<score:
		score_run+=100
		$seCh.play()
		return
	if hi_score<score:
		hi_score = score
		new_hi_score = true
		$seBomp.play()
	$EndGame/Plane/NewHi.visible = new_hi_score
	if count_pencil_run[0]<count_pencil[0]:
		count_pencil_run[0] += 1
		$seCh.play()
		return
	if count_pencil_run[1]<count_pencil[1]:
		count_pencil_run[1] += 1
		$seCh.play()
		return
	if count_pencil_run[2]<count_pencil[2]:
		count_pencil_run[2] += 1
		$seCh.play()
		return
	$seCh.stop()
	result_complete = true
	

func _on_OKBtn_pressed():
	if result_complete == false:
		return
	#print_debug("CLOSEEEEEEEEEEEEEEEEEEEEEEEEEEE")
	new_hi_score = false
	result_complete = false
	$EndGame/Plane/NewHi.visible = false
	$Text/TitleName.show()
	$Text/btnNewGame.show()
	$Text/LabelName.show()
	$Text/Help.show()
	$Player.z_index = 1
	scene = "Title"
	$EndGame/Plane.visible = false
	$seOk.play()

func _on_bgm1_finished():
	$bgm1.play()
