extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var screensize = Vector2(480, 720)
var play_se = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if position.y > 0 and play_se == false :
		$seTitleIn.play()
		play_se = true
	if position.y < (screensize.y*0.25):
		position.y += 4
	
