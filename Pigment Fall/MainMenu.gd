extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$"/root/HUD".time = 0
	$"/root/HUD".score = [0,0,0,0]
	$"/root/HUD/Panel".visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if $Label.rect_position.y <= 40:
		$Label.rect_position.y += 4

func _on_Button_pressed():
	$"/root/HUD/Panel".visible = true
	get_tree().change_scene("res://level1.tscn")
