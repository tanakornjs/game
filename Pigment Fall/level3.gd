extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	$SE/EndLevel.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$"/root/HUD".time += delta
	
	


func _on_PotionBlue_area_entered(area):
	if (area.get_name() == 'Player'):
		$TileMap_Blue.set_collision_layer_bit(0,false)
		$TileMap_Blue.set_modulate(Color(0.13,0.13,1,0.4))
		$"/root/HUD".blue_wall_on = false
		
		$TileMap_Red.set_collision_layer_bit(0,true)
		$TileMap_Red.set_modulate(Color(1,0.13,0.13,1))
		$"/root/HUD".red_wall_on = true
		$TileMap_Green.set_collision_layer_bit(0,true)
		$TileMap_Green.set_modulate(Color(0.13,1,0.13,1))
		$"/root/HUD".green_wall_on = true


func _on_PotionRed_area_entered(area):
	if (area.get_name() == 'Player'):
		$TileMap_Red.set_collision_layer_bit(0,false)
		$TileMap_Red.set_modulate(Color(1,0.13,0.13,0.4))
		$"/root/HUD".red_wall_on = false
		
		$TileMap_Blue.set_collision_layer_bit(0,true)
		$TileMap_Blue.set_modulate(Color(0.13,0.13,1,1))
		$"/root/HUD".blue_wall_on = true
		$TileMap_Green.set_collision_layer_bit(0,true)
		$TileMap_Green.set_modulate(Color(0.13,1,0.13,1))
		$"/root/HUD".green_wall_on = true


func _on_Player_area_entered(area):
	if (area.get_name() == 'coin1'):
		$SE/hitcoin.play()
		$"/root/HUD".score[0] += 1
	if (area.get_name() == 'coin2') and !$"/root/HUD".red_wall_on:
		$SE/hitcoin.play()
		$"/root/HUD".score[1] += 1
	if (area.get_name() == 'coin3') and !$"/root/HUD".blue_wall_on:
		$SE/hitcoin.play()
		$"/root/HUD".score[2] += 1
	if (area.get_name() == 'coin4') and !$"/root/HUD".green_wall_on:
		$SE/hitcoin.play()
		$"/root/HUD".score[3] += 1
	if (area.get_name() == 'PotionRed'):
		$SE/water.play()
	if (area.get_name() == 'PotionBlue'):
		$SE/water.play()
	if (area.get_name() == 'PotionGreen') or (area.get_name() == 'PotionGreen2'): 
		$SE/water.play()


func _on_EndArea_area_entered(area):
	if (area.get_name() == 'Player'):
		$SE/bbggmm.stop()
		get_tree().change_scene("res://GameEnd.tscn")
		$"/root/HUD".reset_level()


func _on_PotionGreen_area_entered(area):
	if (area.get_name() == 'Player'):
		$TileMap_Green.set_collision_layer_bit(0,false)
		$TileMap_Green.set_modulate(Color(0.13,1,0.13,0.4))
		$"/root/HUD".green_wall_on = false
		
		$TileMap_Red.set_collision_layer_bit(0,true)
		$TileMap_Red.set_modulate(Color(1,0.13,0.13,1))
		$"/root/HUD".red_wall_on = true
		$TileMap_Blue.set_collision_layer_bit(0,true)
		$TileMap_Blue.set_modulate(Color(0.13,0.13,1,1))
		$"/root/HUD".blue_wall_on = true

