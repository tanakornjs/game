extends Node2D

var o 

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if $"/root/HUD".blue_wall_on:
		o = 0.4
	else:
		o = 1		
	$coin3/Sprite.set_modulate(Color(1,1,1, o ))


func _on_coin3_area_entered(area):
	if (area.get_name() == 'Player') and o == 1 :
		queue_free()
