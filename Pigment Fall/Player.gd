extends Area2D

var speed=200
var velocity = Vector2()
func _ready():
	pass

func get_input():
	velocity = Vector2()
	if Input.is_action_pressed("ui_up") and ( $RayCastUp.is_colliding() == false ):
		velocity.y -= 1
		$AnimationPlayer.play("walk_left")
		
	if Input.is_action_pressed("ui_down") and ( $RayCastDown.is_colliding() == false ) :
		velocity.y += 1
		$AnimationPlayer.play("walk_right")
		
	if Input.is_action_pressed("ui_left") and ( $RayCastLeft.is_colliding() == false ) :
		$AnimationPlayer.play("walk_left")
		velocity.x -= 1
		
	if Input.is_action_pressed("ui_right") and ( $RayCastRight.is_colliding() == false ) :
		$AnimationPlayer.play("walk_right")
		velocity.x += 1
		
	if Input.is_action_just_released("ui_right") or Input.is_action_just_released("ui_left") or Input.is_action_just_released("ui_down") or Input.is_action_just_released("ui_up"):
		$AnimationPlayer.play("idle")
		
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed
	

func _process(delta):
	get_input()
	position += velocity * delta
