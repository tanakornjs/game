extends Node2D


func _ready():
	pass

func _process(delta):
	$"/root/HUD".time += delta


func _on_PotionRed_area_entered(area):
	if (area.get_name() == 'Player'):
		$TileMap_Red.set_collision_layer_bit(0,false)
		$TileMap_Red.set_modulate(Color(1,0.13,0.13,0.4))
		$"/root/HUD".red_wall_on = false


func _on_Player_area_entered(area):
	if (area.get_name() == 'coin1'):
		$SE/hitcoin.play()
		$"/root/HUD".score[0] += 1
	if (area.get_name() == 'coin2') and !$"/root/HUD".red_wall_on:
		$SE/hitcoin.play()
		$"/root/HUD".score[1] += 1
	if (area.get_name() == 'PotionRed'):
		$SE/water.play()


func _on_EndArea_area_entered(area):
	if (area.get_name() == 'Player'):
		get_tree().change_scene("res://level2.tscn")
		$"/root/HUD".reset_level()

