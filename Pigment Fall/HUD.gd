extends CanvasLayer

var time = 0
var score = [0,0,0,0]

var red_wall_on = true
var blue_wall_on = true
var green_wall_on = true

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$Panel/Time_s.text = str(int(time))
	$Panel/score1.text = str(score[0])
	$Panel/score2.text = str(score[1])
	$Panel/score3.text = str(score[2])
	$Panel/score4.text = str(score[3])
	
func reset_level():
	red_wall_on = true
	blue_wall_on = true
	green_wall_on = true
