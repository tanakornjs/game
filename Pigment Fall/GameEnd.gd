extends Control

# Declare member variables here. Examples:
#var score_run = [$"/root/HUD".score[0],$"/root/HUD".score[1],$"/root/HUD".score[2],$"/root/HUD".score[3]]
var score_run = [0,0,0,0]
var color = [0,0,0,0]
var color_score = [0,0,0,0]
var max_score = [42,32,32,32]
var c
var wait_time = 0
var scale_x = 1.5
var scale_y = 1.5
var rank = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	$"/root/HUD/Panel".visible = false
	$rank.visible = false
	$btnTitle.hide()
	wait_time = 6
	scale_x = 4
	scale_y = 4
	rank = cal_rank()
	$rank.text = "Rank " + rank
	$Panel/Time_s.text = str(int($"/root/HUD".time))
	$adaWhite.set_modulate(Color(1,1,1,0))
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$Panel/score1.text = str(score_run[0])
	$Panel/score2.text = str(score_run[1])
	$Panel/score3.text = str(score_run[2])
	$Panel/score4.text = str(score_run[3])
	c = Color(color[0],color[1],color[2],color[3])
	$ada.set_modulate(c)
	if wait_time > 0:
		wait_time -= 1
		return		
	if score_run[0] < $"/root/HUD".score[0]:
		color_score[3] += 1
		score_run[0] += 1
		color[3] = (float(color_score[3]) / float(max_score[0]) ) 
		$adaWhite.set_modulate(Color(1,1,1,color[3]))
		$seKey.play()
		wait_time = 2
		return
	if score_run[1] < $"/root/HUD".score[1]:
		color_score[0] += 1
		score_run[1] += 1
		color[0] = (float(color_score[0]) / float(max_score[1]) ) 
		$seKey.play()
		wait_time = 2
		return
	if score_run[2] < $"/root/HUD".score[2]:
		color_score[2] += 1
		score_run[2] += 1
		color[1] = (float(color_score[2]) / float(max_score[2]) ) 
		$seKey.play()
		wait_time = 2
		return
	if score_run[3] < $"/root/HUD".score[3]:
		color_score[1] += 1
		score_run[3] += 1
		color[2] = (float(color_score[1]) / float(max_score[3]) ) 
		$seKey.play()
		wait_time = 2
		return	
	$seKey.stop()
	$rank.visible = true
	$rank.set_scale(Vector2(scale_x,scale_y))
	if ( scale_x > 1.0 ):
		scale_x -= 0.05
		scale_y -= 0.05
		return
	else:
		$btnTitle.show()

func cal_rank():
	var point = 0
	point += $"/root/HUD".score[0]
	point += $"/root/HUD".score[1]
	point += $"/root/HUD".score[2]
	point += $"/root/HUD".score[3]
	var max_point = max_score[0] + max_score[1] + max_score[2] + max_score[3]
	point = float(point) / float(max_point) * 100.0
	var time_score = 30
	if $"/root/HUD".time > 180:
		time_score = 0
	var get_rank = int(point*0.7) + time_score
	var r = ""
	if get_rank == 100:
		r = "S"
		$rank.set_modulate(Color(1,1,1,1))
	elif get_rank > 80:
		r = "A"
		$rank.set_modulate(Color(1,0.13,0.13,1))
	elif get_rank > 60:
		r = "B"
		$rank.set_modulate(Color(0.13,1,0.13,1))
	elif get_rank > 40:
		r = "C"
		$rank.set_modulate(Color(0.13,0.13,1,1))
	elif get_rank > 20:
		r = "E"
		$rank.set_modulate(Color(0.68,0.68,0.68,1))
	else:
		r = "F"
		$rank.set_modulate(Color(0.13,0.13,0.13,0.68))
	return r
	
func _on_btnTitle_pressed():
	get_tree().change_scene("res://MainMenu.tscn")
