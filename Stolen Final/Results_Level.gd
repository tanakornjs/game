extends Control

# Declare member variables here. Examples:
# var a = 2
var opacity = 0
var score_show = 0
var room_show = "CS 101"

var room_show1 = 1
var room_show2 = 1
var update_complete = false

var send_score = false
var nameplayer = ""

var time_wait = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	send_score = false
	time_wait = 0
	$node.modulate = Color(1, 1, 1, opacity)
	nameplayer = "จอมโจร" + $"/root/HUD".namePlayer
	$GameJoltAPI.auth_user('gUHmW3', 'jelato')
	$GameJoltAPI.open_session()
	$"/root/Core_Sound/se_win".play()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$ParallaxBackground/BGCity.motion_offset.x += 0.625
	$ParallaxBackground/BGSky.motion_offset.x += 0.125
	$node/labName.text = nameplayer
	$node/labscore.text = str(score_show)
	update_levelname()
	if send_score == false and update_complete:
		$GameJoltAPI.add_guest_score(str(score_show)+" sheet(s)", score_show, nameplayer, 458149)
		$"/root/HUD".add_gold(score_show)
		send_score = true
	$node/btnNameOk.visible = update_complete
	$node/btnReplay.visible = update_complete
	if opacity < 1:
		opacity += 0.01
		$node.modulate = Color(1, 1, 1, opacity)
		return
	if time_wait < 1.5:
		time_wait += delta
		return
	if score_show < $"/root/HUD".score:
		score_show += 1
		$"/root/Core_Sound/se_scorerun".play()
		return
	if room_show2 < $"/root/HUD".level_rm2:
		$"/root/Core_Sound/se_scorerun".play()
		if room_show1 < 10:
			room_show1 += 1
		else:
			room_show1 = 1
			room_show2 += 1
		return
	else:
		if room_show1 < $"/root/HUD".level_rm1:
			room_show1 += 1
			return
	update_complete = true

func update_levelname():
	room_show = "CS "
	room_show += str(room_show2)
	if room_show1 < 10 :
		room_show += "0"+str(room_show1)
	else:
		room_show += str(room_show1)
	$node/labroom.text = room_show

func _on_btnNameOk_pressed():
	$"/root/Core_Sound/ms_Night Prowler".stop()
	$"/root/HUD".show_hud_user()
	get_tree().change_scene("res://Main.tscn")

func _on_btnReplay_pressed():
	get_tree().change_scene("res://Level.tscn")
