extends Sprite

# Declare member variables here. Examples:
var popup_time = 0
var popup_status = 0
var popup_on = false
var opacity = 0
var popup_show = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if popup_on:
		if opacity < 1:
			opacity += 0.3
			self.modulate = Color(1, 1, 1, opacity)
		if popup_status==0:
			if self.position.y >= 450:
				self.position.y -= 15
			else:
				popup_status = 1
		else:
			if self.position.y < 512:
				self.position.y += 15
			else:
				popup_on = false
	else:
		if popup_time > 1:
			self.visible = false
			popup_show = false
		else:
			popup_time += delta

func popup():
	popup_time = 0
	popup_status = 0
	opacity = 0
	popup_on = true
	popup_show = true
	self.visible = true