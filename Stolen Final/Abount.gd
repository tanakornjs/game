extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$ParallaxBackground/BGCity.motion_offset.x += 0.625
	$ParallaxBackground/BGSky.motion_offset.x += 0.125
	

func _on_btnHome_pressed():
	$"/root/HUD".show_hud_user()
	get_tree().change_scene("res://Main.tscn")
