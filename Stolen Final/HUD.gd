extends CanvasLayer

# Declare member variables here. Examples:
var score = 0
var time = 0
var level_rm1 = 1
var level_rm2 = 1

var process = ''
var updatename_fadein = false
var updatename_fadeout = false
var namenotok_time = 0

var score_file = "user://highscore.save"
var highscore

func load_score():
	print("load score")
	var f = File.new()
	if f.file_exists(score_file):
		f.open(score_file, File.READ)
		highscore = f.get_var()
		f.close()
	else:
		highscore = 0

func save_score():
	var f = File.new()
	f.open(score_file, File.WRITE)
	f.store_var(highscore)
	f.close()

var gold_file = "user://gold.save"
var gold

func load_gold():
	print("load gold")
	var f = File.new()
	if f.file_exists(gold_file):
		f.open(gold_file, File.READ)
		gold = f.get_var()
		f.close()
	else:
		gold = 0

func save_gold():
	var f = File.new()
	f.open(gold_file, File.WRITE)
	f.store_var(gold)
	f.close()

var name_file = "user://name.save"
var namePlayer
	
func load_name():
	var f = File.new()
	if f.file_exists(name_file):
		f.open(name_file, File.READ)
		namePlayer = f.get_var()
		f.close()
	else:
		namePlayer = ""
		call_set_name()

func save_name():
	var f = File.new()
	f.open(name_file, File.WRITE)
	f.store_var(namePlayer)
	f.close()

# Called when the node enters the scene tree for the first time.
func _ready():
	load_score()
	load_gold()
	load_name()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$Score.text = "Score : " + str(score)
	$Time.text = "Time : " + str(int(time))
	$CanvasUser/PanelGold/LabelGold.text = str(gold)
	if namePlayer != "":
		$CanvasUser/PanelNameUI/lblName0.text = "จอมโจร" + namePlayer
	else:
		$CanvasUser/PanelNameUI/lblName0.text = ""
	# ตัวอักษรต้องมีขนาด 3 - 15 ตัว
	if $LayerNaming/PanelsetName/LabelName3.visible:
		if $LayerNaming/PanelsetName/LabelName3.rect_scale.x < 1:
			$LayerNaming/PanelsetName/LabelName3.rect_scale.x += 0.02
			$LayerNaming/PanelsetName/LabelName3.rect_scale.y += 0.02
			return
		if namenotok_time > 2:
			$LayerNaming/PanelsetName/LabelName3.visible = false
			namenotok_time = 0
		else:
			namenotok_time += delta
	if updatename_fadein:
		if ($LayerNaming/PanelsetName.rect_scale.x < 1 ):
			$LayerNaming/PanelsetName.rect_scale.x += 0.02
			$LayerNaming/PanelsetName.rect_scale.y += 0.02
			return
		else:
			updatename_fadein = false
	if updatename_fadeout:
		if ($LayerNaming/PanelsetName.rect_scale.x > 0 ):
			$LayerNaming/PanelsetName.rect_scale.x -= 0.02
			$LayerNaming/PanelsetName.rect_scale.y -= 0.02
			return
		else:
			$LayerNaming/PanelsetName.visible = false
			updatename_fadeout = false

func close_hud_level():
	$Score.visible = false
	$Time.visible = false

func show_hud_level():
	pass
	#$Score.visible = true
	#$Time.visible = true

func close_hud_user():
	$CanvasUser/PanelGold.visible = false
	$CanvasUser/PanelNameUI.visible = false

func show_hud_user():
	$CanvasUser/PanelGold.visible = true
	$CanvasUser/PanelNameUI.visible = true

func call_set_name():
	print_debug("call_set_name")
	$LayerNaming/PanelsetName/nameEdit.text = namePlayer
	$LayerNaming/PanelsetName.rect_scale.x = 0.2
	$LayerNaming/PanelsetName.rect_scale.y = 0.2
	$LayerNaming/PanelsetName.visible = true
	$LayerNaming/PanelBlack.visible = true
	$LayerNaming/PanelBlack.modulate = Color(0, 0, 0, 0.6)
	updatename_fadein = true
	process = "naming"

func close_set_name():
	$LayerNaming/PanelBlack.visible = false
	$LayerNaming/PanelBlack.modulate = Color(0, 0, 0, 0)
	updatename_fadeout = true
	process = "title"

func _on_btnNameOk_pressed():
	var naming = $LayerNaming/PanelsetName/nameEdit.text
	var l = naming.length()
	if l in range(3,15):
		# Name Save
		namePlayer = naming
		save_name()
		close_set_name()
	else:
		$LayerNaming/PanelsetName/LabelName3.visible = true
		$LayerNaming/PanelsetName/LabelName3.rect_size.x = 0.05
		$LayerNaming/PanelsetName/LabelName3.rect_size.y = 0.05

func _on_btnRename_pressed():
	call_set_name()

func add_gold(g):
	gold += g
	save_gold()
	
