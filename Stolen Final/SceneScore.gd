extends Node

var score_get = null
var score_show = false

var responseBody
var jsonParseError

# Called when the node enters the scene tree for the first time.
func _ready():
	$GameJoltAPI.auth_user('gUHmW3', 'jelato')
	$GameJoltAPI.open_session()
	#$GameJoltAPI.set_data("score", score, false)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$ParallaxBackground/BGCity.motion_offset.x += 0.625
	$ParallaxBackground/BGSky.motion_offset.x += 0.125
	if score_get == null:
		$GameJoltAPI.fetch_global_scores(10, 458149, 0, null)
	else:
		show_score()

func _on_GameJoltAPI_api_scores_added(success):
	print_debug("Score add !!!!!!!!!!!!!!!!!!!!!")

func _on_GameJoltAPI_request_completed(result, response_code, headers, body):
	#print_debug(result, response_code, headers, body)
	#responseBody = body.get_string_from_utf8()
	#print_debug("body : " ,body.get_string_from_utf8())
	responseBody = body.get_string_from_utf8()
	responseBody = JSON.parse(responseBody)
	jsonParseError = responseBody.error
	if jsonParseError == OK:
		responseBody = responseBody.result['response']
		if responseBody['success'] == 'true':
			#print_debug("SCORE : " ,responseBody['scores'])
			score_get = responseBody['scores']

func show_score():
	if score_show:
		return
	var text_name = ""
	var text_score = ""
	for i in score_get:
		print_debug("Score : " , i)
		text_name += i['guest'] + "\n"
		text_score += i['score'] + "\n"
	$Labelname.text = text_name
	$Labelscore.text = text_score
	score_show = true

func _on_btnHome_pressed():
	$"/root/HUD".show_hud_user()
	get_tree().change_scene("res://Main.tscn")
