extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_ms_attheendofhope_finished():
	$ms_attheendofhope.play()

func _on_ms_awesomeness_finished():
	$ms_awesomeness.play()

func _on_ms_Christmas_synths_finished():
	$"ms_Christmas synths".play()

func _on_ms_Night_Prowler_finished():
	$"ms_Night Prowler".play()
