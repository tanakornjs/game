extends Node2D

# Declare member variables here. Examples:
# var a = 2
var time_ui_rotated = 0.0
var time_ui_rotated_state = 0
var max_times = 180
var time = 180
var time_percent = 100
var time_mark_width = 441
var time_mark_x = 565.615

var timeup = false
var pause_game = false

var passwordAns = ""
var passwordShow = ""
var passwordCursor = ""
var passwordAns_cursor = 0
var passwordAns_update_time = 0

var time_count = 4
var countdowning = true

var password_question = ""

var score = 0
var text_room = "CS101"

var update_room_test_status = 0
var update_room_dis = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	time_mark_x = $time_bar03.position.x
	$passwordAns.text = passwordAns
	$"/root/HUD".score = 0
	$"/root/HUD".level_rm1 = 1
	$"/root/HUD".level_rm2 = 1
	make_password_question()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if pause_game:
		return
	check_timeup()
	if timeup:
		return
	$ParallaxBackground/BGCity.motion_offset.x += 0.625
	$ParallaxBackground/BGSky.motion_offset.x += 0.125
	$UILevel/labScore.text = str($"/root/HUD".score)
	update_room()
	update_levelname()
	if countdowning:
		countdown_time(delta)
		return
	time_update(delta)
	password_update_cursor(delta)

func update_room():
	$PlayerRun/Player/AnimationPlayer.play("walk_right")
	if update_room_dis > 0:
		$bgRoom/pl1.motion_offset.x += 1
		$bgRoom/pl2.motion_offset.x += 1
		update_room_dis -= 1
	if update_room_test_status == 0:
		if $bgRoom/pl2/icon_test01.scale.x < 0.25:
			$bgRoom/pl2/icon_test01.position.y -= 1
			$bgRoom/pl2/icon_test01.scale.x += 0.005
			$bgRoom/pl2/icon_test01.scale.y += 0.005
			$bgRoom/pl2/icon_test02.position.y -= 1
			$bgRoom/pl2/icon_test02.scale.x += 0.005
			$bgRoom/pl2/icon_test02.scale.y += 0.005
		else:
			update_room_test_status = 1
	else:
		if $bgRoom/pl2/icon_test01.scale.x > 0.2:
			$bgRoom/pl2/icon_test01.position.y += 1
			$bgRoom/pl2/icon_test01.scale.x -= 0.005
			$bgRoom/pl2/icon_test01.scale.y -= 0.005
			$bgRoom/pl2/icon_test02.position.y += 1
			$bgRoom/pl2/icon_test02.scale.x -= 0.005
			$bgRoom/pl2/icon_test02.scale.y -= 0.005
		else:
			update_room_test_status = 0
	
func update_levelname():
	text_room = "CS "
	text_room += str($"/root/HUD".level_rm2)
	if $"/root/HUD".level_rm1 < 10 :
		text_room += "0"+str($"/root/HUD".level_rm1)
	else:
		text_room += str($"/root/HUD".level_rm1)
	$UILevel/labLevel.text = text_room
	
func make_password_question():
	randomize()
	var pwd1 = randi() % 9
	var pwd2 = randi() % 9
	var pwd3 = randi() % 9
	while( pwd1==pwd2 ):
		pwd2 = randi() % 9
	while( pwd1==pwd3 or pwd2==pwd3):
		pwd3 = randi() % 9
	password_question = str(pwd1) + str(pwd2) + str(pwd3)
	#print_debug(password_question)
	
func countdown_time(delta):
	$CanvasF/PanelBlack.visible = true
	time_count -= delta
	$CanvasF/Time/countdown1.visible = int(time_count) == 1
	$CanvasF/Time/countdown2.visible = int(time_count) == 2
	$CanvasF/Time/countdown3.visible = int(time_count) == 3
	if time_count < 1:
		countdowning = false
		$CanvasF/PanelBlack.visible = false

func time_update(delta):
	# Time_animate
	$time_ui.rotation_degrees = time_ui_rotated
	if time_ui_rotated_state == 0:
		time_ui_rotated -= 0.2
		if time_ui_rotated < -25:
			time_ui_rotated_state = 1
	else:
		time_ui_rotated += 0.2
		if time_ui_rotated > 25:
			time_ui_rotated_state = 0
	# Time --
	if time >0:
		time -= delta
		time_percent = ( time * 100 / max_times )
		$time_bar02.scale.x = time_percent / 100
		$time_bar03.position.x = (time_mark_x-time_mark_width)+(time_percent*time_mark_width/100)

func check_timeup():
	if time <= 0 and timeup == false:
		$CanvasAlert/complete02.popup()
		timeup = true
		$CanvasAlert/PanelBlack2.visible = true
	if timeup and $CanvasAlert/complete02.popup_show == false:
		# ไปหน้าสรุปผล
		get_tree().change_scene("res://Results_Level.tscn")

func password_update_cursor(delta):
	passwordShow = passwordAns + passwordCursor
	$passwordAns.text = passwordShow
	if passwordAns_update_time > 0.5:
		# Update_Bink
		if passwordCursor == "":
			passwordCursor = "_"
		else:
			passwordCursor = ""
		passwordAns_update_time = 0
	else:
		passwordAns_update_time += delta

func password_check():
	var l = passwordAns.length()
	if l >= 3:
		# ans
		if passwordAns == password_question:
			guess_right()
		else:
			guess_wrong()

func guess_right():
	$CanvasAlert/complete01.popup()
	#score += 1
	$"/root/HUD".score += 1
	if $"/root/HUD".level_rm1 == 10:
		$"/root/HUD".level_rm2 += 1
		$"/root/HUD".level_rm1 = 1
	else:
		$"/root/HUD".level_rm1 += 1
	update_room_dis = 235
	resetQue()

func guess_wrong():
	var w = 0
	var r_notplace = 0
	var r = 0
	
	var pw_q = []
	for c in password_question:
		pw_q.append(c)
	
	for i in range(0,len(password_question)):
		if password_question[i] == passwordAns[i]:
			r += 1
		else:
			if pw_q.has(passwordAns[i]):
				r_notplace += 1
			else:
				w += 1
	
	var label = Label.new()
	label.set_text(passwordAns)
	var font = get_node("passwordguide/ScrollContainer/VBox/labTop").get_font("font")
	label.set("custom_fonts/font",font)
	
	var hbox = HBoxContainer.new()
	hbox.set_alignment(2)
	hbox.set_h_size_flags(2)
	if r > 0:
		var label_hint1 = Label.new()
		label_hint1.set_text( "O " + str(r) )
		label_hint1.set("custom_fonts/font",font)
		label_hint1.set("custom_colors/font_color",Color(0.6,1,0.6))
		hbox.add_child(label_hint1)
	if w > 0:
		var label_hint2 = Label.new()
		label_hint2.set_text( "X " + str(w) )
		label_hint2.set("custom_fonts/font",font)
		label_hint2.set("custom_colors/font_color",Color(1,0.6,0.6))
		hbox.add_child(label_hint2)
	if r_notplace > 0:
		var label_hint3 = Label.new()
		label_hint3.set_text( "? " + str(r_notplace) )
		label_hint3.set("custom_fonts/font",font)
		label_hint3.set("custom_colors/font_color",Color(1,1,0.6))
		hbox.add_child(label_hint3)
	
	var vbox = VBoxContainer.new()
	vbox.add_child(label)
	vbox.add_child(hbox)
	var lt = $passwordguide/ScrollContainer/VBox/labTop
	$passwordguide/ScrollContainer/VBox.add_child_below_node(lt,vbox)
	resetAns()

func resetAns():
	passwordAns = ""
	for i in [$btn0,$btn1,$btn2,$btn3,$btn4,$btn5,$btn6,$btn7,$btn8,$btn9]:
		i.disabled = false

func resetQue():
	for i in $passwordguide/ScrollContainer/VBox.get_children():
		if i == get_node("passwordguide/ScrollContainer/VBox/labTop"):
			continue
		i.queue_free()
	#print_debug($passwordguide/ScrollContainer/VBox.get_children())
	make_password_question()
	resetAns()

func _on_btn0_pressed():
	$btn0.disabled = true
	passwordAns += "0"
	password_check()

func _on_btn1_pressed():
	$btn1.disabled = true
	passwordAns += "1"
	password_check()

func _on_btn2_pressed():
	$btn2.disabled = true
	passwordAns += "2"
	password_check()

func _on_btn3_pressed():
	$btn3.disabled = true
	passwordAns += "3"
	password_check()

func _on_btn4_pressed():
	$btn4.disabled = true
	passwordAns += "4"
	password_check()

func _on_btn5_pressed():
	$btn5.disabled = true
	passwordAns += "5"
	password_check()

func _on_btn6_pressed():
	$btn6.disabled = true
	passwordAns += "6"
	password_check()

func _on_btn7_pressed():
	$btn7.disabled = true
	passwordAns += "7"
	password_check()

func _on_btn8_pressed():
	$btn8.disabled = true
	passwordAns += "8"
	password_check()

func _on_btn9_pressed():
	$btn9.disabled = true
	passwordAns += "9"
	password_check()

func _on_btnPause_pressed():
	if countdowning:
		return
	$CanvasF/btnPause.disabled = true
	$CanvasF/PanelBlack.visible = true
	$CanvasF/btnExit.visible = true
	$CanvasF/btnUnpause.visible = true
	$passwordguide/ScrollContainer.visible = false
	pause_game = true

func _on_btnUnpause_pressed():
	pause_game = false
	$CanvasF/btnPause.disabled = false
	$CanvasF/PanelBlack.visible = false
	$CanvasF/btnExit.visible = false
	$CanvasF/btnUnpause.visible = false
	$passwordguide/ScrollContainer.visible = true


func _on_btnExit_pressed():
	get_tree().change_scene("res://Results_Level.tscn")
