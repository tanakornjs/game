extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	$"/root/Core_Sound/ms_attheendofhope".play()
	$"/root/HUD".close_hud_level()
	#load_name()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$ParallaxBackground/BGCity.motion_offset.x += 5
	$ParallaxBackground/BGSky.motion_offset.x += 1


func _on_btnPlayersingle_pressed():
	$"/root/Core_Sound/ms_attheendofhope".stop()
	$"/root/Core_Sound/ms_Night Prowler".play()
	$"/root/HUD".close_hud_user()
	$"/root/HUD".show_hud_level()
	get_tree().change_scene("res://Level.tscn")


func _on_btnscore_pressed():
	$"/root/HUD".close_hud_user()
	get_tree().change_scene("res://SceneScore.tscn")


func _on_btnonline_pressed():
	$"/root/Core_Sound/ms_attheendofhope".stop()
	$"/root/Core_Sound/ms_Christmas synths".play()
	$"/root/HUD".close_hud_user()
	get_tree().change_scene("res://lobby.tscn")


func _on_btnhowtoplay_pressed():
	$"/root/HUD".close_hud_user()
	get_tree().change_scene("res://howtoplay.tscn")


func _on_btncontact_pressed():
	$"/root/HUD".close_hud_user()
	get_tree().change_scene("res://Abount.tscn")


func _on_btnshop_pressed():
	$"/root/Core_Sound/ms_attheendofhope".stop()
	$"/root/Core_Sound/ms_awesomeness".play()	
	get_tree().change_scene("res://Shop.tscn")
